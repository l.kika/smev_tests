package ru.profit.smev.tests;

public class SignatureProcessorException extends Throwable {
    public SignatureProcessorException(String s) {
        super(s);
    }
}
