package ru.profit.smev.tests;

import com.sun.org.apache.xpath.internal.XPathAPI;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.xml.security.exceptions.AlgorithmAlreadyRegisteredException;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.signature.XMLSignatureException;

import org.apache.xml.security.transforms.InvalidTransformException;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.transforms.params.XPath2FilterContainer;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.CryptoPro.JCPxml.xmldsig.JCPXMLDSigInit;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Signer {
    private static final String XMLDSIG_MORE_GOSTR34102001_GOSTR3411 = "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34102012-gostr34112012-256";
    private static final String XMLDSIG_MORE_GOSTR3411 = "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34112012-256";
    private static final String CANONICALIZATION_METHOD = "http://www.w3.org/2001/10/xml-exc-c14n#";
    private static final String COULD_NOT_FIND_XML_ELEMENT_NAME = "ERROR! Could not find xmlElementName = ";
    private static final String GRID = "#";
    private static final String XML_SIGNATURE_ERROR = "xmlDSignature ERROR: ";

    private static final Logger LOGGER = Logger.getLogger(Signer.class.getName());
    private static final String IGNORE_LINE_BREAKS_FIELD = "ignoreLineBreaks";

    public static void init2() {
        // Инициализация Transforms.
        com.sun.org.apache.xml.internal.security.Init.init();
        // Инициализация сервис-провайдера.
        if(!JCPXMLDSigInit.isInitialized()) {
            JCPXMLDSigInit.init();
        }
    }

    public static void init() throws ClassNotFoundException, AlgorithmAlreadyRegisteredException, InvalidTransformException {
        // Инициализация сервис-провайдера.
        if(!JCPXMLDSigInit.isInitialized()) {
            JCPXMLDSigInit.init();
        }

        Transform.register(SmevTransformSpi.ALGORITHM_URN, SmevTransformSpi.class.getName());
        santuarioIgnoreLineBreaks(true);
        LOGGER.log(Level.INFO, "SmevTransformSpi has been initialized");
    }

    /**
     * Apache Santuario privileged switch IgnoreLineBreaks property
     *
     * @param mode
     */
    private static void santuarioIgnoreLineBreaks(Boolean mode) {
        try {
            final Boolean currMode = mode;
            AccessController.doPrivileged(new PrivilegedExceptionAction<Boolean>() {

                public Boolean run() throws Exception {
                    Field f = XMLUtils.class.getDeclaredField(IGNORE_LINE_BREAKS_FIELD);
                    f.setAccessible(true);
                    f.set(null, currMode);
                    return false;
                }
            });

        } catch (Exception e) {
            LOGGER.warning("santuarioIgnoreLineBreaks " + ExceptionUtils.getFullStackTrace(e));
        }
    }

    public static byte[] sign(
            byte[] data, // XML сообщение в виде массива байтов
            String xmlElementName, // имя элемента в XML вместе с префиксом, в который следует добавить подпись, для СМЭВ-3 в общем случае "ns2:CallerInformationSystemSignature"
            String xmlElementID, // ID элемента в XML (если присутствует) вместе с префиксом, на который следует поставить подпись, для СМЭВ-3 в общем случае "SIGNED_BY_CONSUMER"
            X509Certificate certificate, // сертификат открытого ключа проверки подписи
            PrivateKey privateKey // закрытый ключ подписи
    ) throws ParserConfigurationException, XMLSecurityException, SAXException, SignatureProcessorException {
        ByteArrayOutputStream bais;
        try {
            // инициализация объекта чтения XML-документа
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

            // установка флага, определяющего игнорирование пробелов в
            // содержимом элементов при обработке XML-документа
            dbf.setIgnoringElementContentWhitespace(true);

            // установка флага, определяющего преобразование узлов CDATA в
            // текстовые узлы при обработке XML-документа
            dbf.setCoalescing(true);

            // установка флага, определяющего поддержку пространств имен при
            // обработке XML-документа
            dbf.setNamespaceAware(true);

            // загрузка содержимого подписываемого документа на основе
            // установленных флагами правил из массива байтов data
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();

            Document doc = documentBuilder.parse(new ByteArrayInputStream(data));

            /*
             * Добавление узла подписи <ds:Signature> в загруженный XML-документ
             */

            // алгоритм подписи (ГОСТ Р 34.10-2001)
            final String signMethod = XMLDSIG_MORE_GOSTR34102001_GOSTR3411;

            // алгоритм хеширования, используемый при подписи (ГОСТ Р 34.11-94)
            final String digestMethod = XMLDSIG_MORE_GOSTR3411;

            final String canonicalizationMethod = CANONICALIZATION_METHOD;


            //String[][] filters = {{XPath2FilterContainer.SUBTRACT, DS_SIGNATURE}};
            //String sigId = SIG_ID;

            // инициализация объекта формирования ЭЦП в соответствии с
            // алгоритмом ГОСТ Р 34.10-2001
            XMLSignature sig = new XMLSignature(doc, "", signMethod, canonicalizationMethod);

            // определение идентификатора первого узла подписи

            //sig.setId(sigId);

            // получение корневого узла XML-документа
            Element anElement = null;
            if (xmlElementName == null) {
                anElement = doc.getDocumentElement();
            } else {
                NodeList nodeList = doc.getElementsByTagName(xmlElementName);
                anElement = (Element) ((NodeList) nodeList).item(0);
            }
            // добавление в корневой узел XML-документа узла подписи
            if (anElement != null) {
                anElement.appendChild(sig.getElement());
            } else {
                throw new SignatureProcessorException(COULD_NOT_FIND_XML_ELEMENT_NAME + xmlElementName);
            }

            /*
             * Определение правил работы с XML-документом и добавление в узел подписи этих
             * правил
             */

            // создание узла преобразований <ds:Transforms> обрабатываемого
            // XML-документа
            Transforms transforms = new Transforms(doc);

            // добавление в узел преобразований правил работы с документом
            transforms.addTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);
            transforms.addTransform(SmevTransformSpi.ALGORITHM_URN);

            // добавление в узел подписи ссылок (узла <ds:Reference>),
            // определяющих правила работы с
            // XML-документом (обрабатывается текущий документ с заданными в
            // узле <ds:Transforms> правилами
            // и заданным алгоритмом хеширования)
            sig.addDocument(xmlElementID == null ? "" : GRID + xmlElementID, transforms, digestMethod);

            /*
             * Создание подписи всего содержимого XML-документа на основе закрытого ключа,
             * заданных правил и алгоритмов
             */

            // создание внутри узла подписи узла <ds:KeyInfo> информации об
            // открытом ключе на основе
            // сертификата
            sig.addKeyInfo(certificate);

            // создание подписи XML-документа
            sig.sign(privateKey);

            // определение потока, в который осуществляется запись подписанного
            // XML-документа
            bais = new ByteArrayOutputStream();

            // инициализация объекта копирования содержимого XML-документа в
            // поток
            TransformerFactory tf = TransformerFactory.newInstance();

            // создание объекта копирования содержимого XML-документа в поток
            Transformer trans = tf.newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            // копирование содержимого XML-документа в поток
            trans.transform(new DOMSource(doc), new StreamResult(bais));
            bais.close();
        } catch (TransformationException e) {
            throw new SignatureProcessorException("TransformationException " + XML_SIGNATURE_ERROR + e.getMessage());
        } catch (XMLSignatureException e) {
            throw new SignatureProcessorException("XMLSignatureException " + XML_SIGNATURE_ERROR + e.getMessage());
        } catch (TransformerException e) {
            throw new SignatureProcessorException("TransformerException " + XML_SIGNATURE_ERROR + e.getMessage());
        } catch (IOException e) {
            throw new SignatureProcessorException("IOException " + XML_SIGNATURE_ERROR + e.getMessage());
        } catch (XMLSecurityException e) {
            throw new SignatureProcessorException("XMLSecurityException " + XML_SIGNATURE_ERROR + e.getMessage());
        } catch (SAXException e) {
            throw new SignatureProcessorException("SAXException " + XML_SIGNATURE_ERROR + e.getMessage());
        } catch (ParserConfigurationException e) {
            throw new SignatureProcessorException(
                    "ParserConfigurationException " + XML_SIGNATURE_ERROR + e.getMessage());
        }
        return bais.toByteArray();
    }

    public static byte[] sign2(
            InputStream data, // XML сообщение в виде массива байтов
            X509Certificate cert, // сертификат открытого ключа проверки подписи
            PrivateKey privateKey // закрытый ключ подписи
    ) throws SignatureProcessorException {
        MessageFactory mf = null;
        try {
            mf = MessageFactory.newInstance();
            SOAPMessage message = mf.createMessage();
            SOAPPart soapPart = message.getSOAPPart();
            soapPart.setContent(new StreamSource(data));

            message.getSOAPPart().getEnvelope().addNamespaceDeclaration("ds", "http://www.w3.org/2000/09/xmldsig#");
            Document doc = message.getSOAPPart().getEnvelope().getOwnerDocument();

            WSSecHeader header = new WSSecHeader();
            header.setActor("http://smev.gosuslugi.ru/actors/smev");
            header.setMustUnderstand(false);
            header.insertSecurityHeader(message.getSOAPPart().getEnvelope().getOwnerDocument());
            // Элемент подписи.
            Element token = header.getSecurityHeader();

            // Загрузка провайдера.
            Provider xmlDSigProvider = new ru.CryptoPro.JCPxml.dsig.internal.dom.XMLDSigRI();

            final Transforms transforms = new Transforms(doc);
            transforms.addTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS);
            XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM", xmlDSigProvider);

            List<javax.xml.crypto.dsig.Transform> transformList = new ArrayList<>();
            javax.xml.crypto.dsig.Transform transformC14N = fac.newTransform(Transforms.TRANSFORM_C14N_EXCL_OMIT_COMMENTS, (XMLStructure) null);
            transformList.add(transformC14N);

            // Ссылка на подписываемые данные с алгоритмом хеширования ГОСТ 34.11.
            Reference ref = fac.newReference("#body", fac.newDigestMethod("http://www.w3.org/2001/04/xmldsig-more#gostr3411", null),
                    transformList, null, null);

            SignedInfo si = fac.newSignedInfo(
                    fac.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE, (C14NMethodParameterSpec) null),
                    fac.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411",null),
                    Collections.singletonList(ref));

            // Создаём узел ds:KeyInfo с информацией о сертификате
            KeyInfoFactory kif = fac.getKeyInfoFactory();
            X509Data x509d = kif.newX509Data(Collections.singletonList(cert));
            KeyInfo ki = kif.newKeyInfo(Collections.singletonList(x509d));

            // Подписываем данные в элементе token
            javax.xml.crypto.dsig.XMLSignature sig = fac.newXMLSignature(si, ki);
            DOMSignContext signContext = new DOMSignContext((Key) privateKey, token);
            sig.sign(signContext);

//            // Следующий этап — поместить узел ds:Signature и сертификат (X509Certificate) в узел wsse:Security,
//            // причём сертификат нужно удалить из ds:KeyInfo и
//            // оставить там ссылку на wsse:BinarySecurityToken с сертификатом
//
//            // Узел подписи Signature.
//            Element sigE = (Element) XPathAPI.selectSingleNode(signContext.getParent(), "//ds:Signature");
//            // Блок данных KeyInfo.
//            Node keyE = XPathAPI.selectSingleNode(sigE, "//ds:KeyInfo", sigE);
//            // Элемент SenderCertificate, который должен содержать сертификат.
//            Element cerVal = (Element) XPathAPI.selectSingleNode(token, "//*[@wsu:Id='SenderCertificate']");
//            cerVal.setTextContent(XPathAPI.selectSingleNode(keyE, "//ds:X509Certificate", keyE).getFirstChild().getNodeValue());
//            // Удаляем содержимое KeyInfo
//            keyE.removeChild(XPathAPI.selectSingleNode(keyE, "//ds:X509Data", keyE));
//            NodeList chl = keyE.getChildNodes();
//            for (int i = 0; i < chl.getLength(); i++) {
//                keyE.removeChild(chl.item(i));
//            }
//            // Узел KeyInfo содержит указание на проверку подписи с помощью сертификата SenderCertificate.
//            Node str = keyE.appendChild(doc.createElementNS("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "wsse:SecurityTokenReference"));
//            Element strRef = (Element)str.appendChild(doc.createElementNS("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "wsse:Reference"));
//            strRef.setAttribute("ValueType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3");
//            strRef.setAttribute("URI", "#SenderCertificate");
//            header.getSecurityHeader().appendChild(sigE);

            // output document
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            org.apache.xml.security.utils.XMLUtils.outputDOM(doc, baos, true);
            return baos.toByteArray();
        } catch (SOAPException |
                TransformationException |
                NoSuchAlgorithmException |
                InvalidAlgorithmParameterException |
                //TransformerException |
                MarshalException |
                javax.xml.crypto.dsig.XMLSignatureException e) {
            e.printStackTrace();
            throw new SignatureProcessorException(e.getMessage());
        }
    }
}
