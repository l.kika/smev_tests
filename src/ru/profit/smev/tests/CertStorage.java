package ru.profit.smev.tests;

import ru.CryptoPro.JCP.JCP;

import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class CertStorage {
    private static final String ALIAS = "cded5342-13b3-4771-9c05-a2390c5e7264  - Copy";
    private static final String PASSWORD = "5253001036";

    private static PrivateKey privateKey;
    private static X509Certificate cert;
    private static CertStorage instance;
    public static synchronized CertStorage getInstance() throws CertStorageInitException {
        if (instance != null) {
            return instance;
        }

        instance = new CertStorage();
        try {
            instance.init();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new CertStorageInitException(e.getMessage());
        }
        return instance;
    }

    private CertStorage() {
    }

    private void init() throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, IOException, CertificateException, CertStorageInitException {
        KeyStore keyStore = null;
        // Инициализация ключевого контейнера и получение сертификата и закрытого ключа.
        keyStore = KeyStore.getInstance(JCP.HD_STORE_NAME);
        keyStore.load(null, null);

        this.privateKey = (PrivateKey)keyStore.getKey(ALIAS, PASSWORD.toCharArray());
        cert = (X509Certificate)keyStore.getCertificate(ALIAS);
        if (cert == null) {
            throw new CertStorageInitException("certificate could not be loaded");
        }
    }

    public X509Certificate getCertificate() {
        return CertStorage.cert;
    }

    public PrivateKey getPrivateKey() {
        return CertStorage.privateKey;
    }
}
