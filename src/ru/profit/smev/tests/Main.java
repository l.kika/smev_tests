package ru.profit.smev.tests;

import org.apache.xml.security.exceptions.AlgorithmAlreadyRegisteredException;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.transforms.InvalidTransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Main {
    private static final String messageFile = "data/message.xml";

    public static void main(String[] args) {
        System.out.println("bebe");

        try {
            Signer.init();

            ClassLoader classLoader = Main.class.getClassLoader();
            File file = new File(classLoader.getResource(messageFile).getFile());
            byte[] msg = Files.readAllBytes(file.toPath());
            String content = new String(msg);
            System.out.println(content);

            CertStorage storage = CertStorage.getInstance();

            byte[] res = Signer.sign(msg,
                    "CallerInformationSystemSignature",
                    "SIGNED_BY_CONSUMER",
                    storage.getCertificate(),
                    storage.getPrivateKey()
                    );
            System.out.println("------------------- result --------------");
            Logger logger = LoggerFactory.getLogger(Main.class);
            logger.debug(new String(res));
            System.out.println("------------------ end result -----------");
         } catch (AlgorithmAlreadyRegisteredException e) {
            e.printStackTrace();
        } catch (InvalidTransformException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (XMLSecurityException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (SignatureProcessorException e) {
            e.printStackTrace();
        } catch (CertStorageInitException e) {
            e.printStackTrace();
        }
//        Signer.init2();
//        try {
//            CertStorage storage = CertStorage.getInstance();
//            ClassLoader classLoader = Main.class.getClassLoader();
//
//            File file = new File(classLoader.getResource(messageFile).getFile());
//            byte[] msg = Files.readAllBytes(file.toPath());
//            String content = new String(msg);
//            System.out.println(content);
//
//            byte[] res = Signer.sign2(new ByteArrayInputStream(msg), storage.getCertificate(), storage.getPrivateKey());
//            System.out.println("------------------- result --------------");
//            System.out.println(new String(res));
//            System.out.println("------------------ end result -----------");
//        } catch (CertStorageInitException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (SignatureProcessorException e) {
//            e.printStackTrace();
//        }
    }
}
