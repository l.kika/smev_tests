package ru.profit.smev.tests;

public class CertStorageInitException extends Throwable {
    public CertStorageInitException(String s) {
        super(s);
    }
}
